package main

import (
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"time"
)

type LoginStruct struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RegisterStruct struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Avatar   string `json:"avatar"`
}

type jwtCustomClaims struct {
	Id    int64  `json:"id"`
	Name  string `json:"name"`
	Admin bool   `json:"admin"`
	jwt.StandardClaims
}

func HandleLogin(data LoginStruct) (string, error) {
	validationError := validateStruct(data)
	var user = User{}

	if validationError != nil {
		validationError.Target = "login"

		return "", validationError
	}

	err := Db.QueryRowx(
		"SELECT * FROM user WHERE username = ? AND is_active = 1 LIMIT 1", data.Username).StructScan(&user)

	if (err != nil) || !CheckPasswordHash(data.Password, user.Password) {
		return "", &HttpResponseError{
			Code:    http.StatusBadRequest,
			Message: "login or password is incorrect",
			Target:  "login",
		}
	}

	return generateToken(user)
}

func HandleRegister(data RegisterStruct) (int64, *HttpResponseError) {
	validationError := validateStruct(data)

	if validationError != nil {
		validationError.Target = "register"
		return 0, validationError
	}

	pwd, err := HashPassword(data.Password)

	if err != nil {
		return 0, &HttpResponseError{
			Code:    http.StatusInternalServerError,
			Message: "internal server error",
			Target:  "register",
		}
	}

	data.Password = pwd
	result, err := Db.Exec(
		"INSERT INTO user (username, email, avatar, password) VALUES (?,?,?,?)",
		data.Username,
		data.Email,
		data.Avatar,
		data.Password)

	if err != nil {
		return 0, &HttpResponseError{
			Code:    http.StatusBadRequest,
			Message: "username or email already exists",
			Target:  "register",
		}
	}

	userId, err := result.LastInsertId()

	if err != nil {
		return userId, &HttpResponseError{
			Code:    http.StatusBadRequest,
			Message: "can't register user",
		}
	}

	return userId, nil
}

func generateToken(user User) (string, error) {
	claims := &jwtCustomClaims{
		user.ID,
		user.Username,
		true,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte("secret"))

	if err != nil {
		return "", &HttpResponseError{
			Code:    http.StatusInternalServerError,
			Target:  "login",
			Message: "unable to login user",
		}
	}

	return t, err
}

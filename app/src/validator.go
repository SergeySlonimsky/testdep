package main

import (
	"gopkg.in/go-playground/validator.v9"
	"log"
	"net/http"
)

var validate *validator.Validate

func InitValidator() {
	validate = validator.New()
}

func validateStruct(str interface{}) *HttpResponseError {
	err := validate.Struct(str)

	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return nil
		}

		var httpError HttpResponseError

		httpError.Message = "validation error"
		httpError.Code = http.StatusBadRequest
		log.Println(err.(validator.ValidationErrors))

		for _, err := range err.(validator.ValidationErrors) {
			switch err.ActualTag() {
			case "required":
				httpError.Inner = append(httpError.Inner, HttpResponseError{
					Target:  err.Field(),
					Message: "field should be required",
				})
				break
			case "email":
				httpError.Inner = append(httpError.Inner, HttpResponseError{
					Target:  err.Field(),
					Message: "field should be an email",
				})
				break
			}
		}
		return &httpError
	}
	return nil
}

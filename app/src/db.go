package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
	"os"
	"time"
)

type User struct {
	ID        int64     `json:"id" db:"id"`
	Username  string    `json:"username" db:"username"`
	Email     string    `json:"email" db:"email"`
	Avatar    *string   `json:"avatar" db:"avatar"`
	Password  string    `json:"-" db:"password"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	IsActive  bool      `json:"-" db:"is_active"`
}

type Channel struct {
	ID   int64  `json:"id" db:"id"`
	Name string `json:"id" db:"name"`
}

type Message struct {
	Id        int64     `json:"id" db:"is_active"`
	Text      string    `json:"message" db:"is_active"`
	CreatedAt time.Time `json:"created_at" db:"is_active"`
	IsVisible bool      `json:"visible" db:"is_visible"`
	ChannelId int64     `json:"channel_id" db:"is_active"`
	Channel   Channel   `json:"channel" db:"is_active"`
	SenderId  int64     `json:"sender_id" db:"is_active"`
	Sender    User      `json:"sender" db:"is_active"`
}

var Db *sqlx.DB

func initDB() {
	database := os.Getenv("MYSQL_DATABASE")
	user := "root"
	password := os.Getenv("MYSQL_ROOT_PASSWORD")
	port := "3306"
	connectionString := user + ":" + password + "@(mysql:" + port + ")/" + database + "?parseTime=true"
	db, err := sqlx.Connect("mysql", connectionString)
	if err != nil {
		log.Fatalln(err)
	}

	Db = db
}

package main

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"strconv"
)

type HttpResponseError struct {
	Code    int                 `json:"code"`
	Message string              `json:"message"`
	Target  string              `json:"target"`
	Inner   []HttpResponseError `json:"inner_errors"`
}

func (err *HttpResponseError) Error() string {
	return err.Message
}

func main() {
	initDB()
	InitValidator()
	e := echo.New()
	a := e.Group("/auth")
	a.POST("/login", Login)
	a.POST("/register", Register)

	r := e.Group("/user")
	config := middleware.JWTConfig{
		Claims:     &jwtCustomClaims{},
		SigningKey: []byte("secret"),
	}
	r.Use(middleware.JWTWithConfig(config))
	r.GET("/channels", GetChannels)

	e.Logger.Fatal(e.Start(":1323"))
}

func Login(c echo.Context) error {
	token, err := HandleLogin(LoginStruct{
		Username: c.FormValue("username"),
		Password: c.FormValue("password"),
	})

	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": token,
	})
}

func Register(c echo.Context) error {
	userId, err := HandleRegister(RegisterStruct{
		Username: c.FormValue("username"),
		Password: c.FormValue("password"),
		Email:    c.FormValue("email"),
		Avatar:   c.FormValue("avatar"),
	})

	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	c.Response().Header().Set(echo.HeaderLocation, strconv.Itoa(int(userId)))
	return c.NoContent(http.StatusCreated)
}

func GetChannels(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*jwtCustomClaims)
	fmt.Println(claims)
	userId := claims.Id
	channels, err := HandleGetChannels(userId)
	if err != nil {
		return c.JSON(err.Code, err)
	}
	return c.JSON(http.StatusOK, channels)
}
